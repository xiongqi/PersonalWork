import React, { Component } from 'react';
import { EditorState, convertToRaw, ContentState, convertFromRaw, convertFromHTML } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import { Modal } from 'antd';
import draftToHtml from 'draftjs-to-html';
// import htmlToDraft from 'html-to-draftjs';
import '../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

const content = {
  entityMap: {},
  blocks: [{ key: '637gr', text: 'Initialized from content state.', type: 'unstyled', depth: 0, inlineStyleRanges: [], entityRanges: [], data: {} }]
};
const contentState = convertFromRaw(content);
const sampleMarkup = 'aaa<br />555';
const blocksFromHTML = convertFromHTML(sampleMarkup);
const state = ContentState.createFromBlockArray(blocksFromHTML.contentBlocks, blocksFromHTML.entityMap);
export default class EditorConvertToHTML extends Component {
  state = {
    editorState: EditorState.createWithContent(state),
    contentState
  };

  onEditorStateChange: Function = editorState => {
    this.setState({
      editorState
    });
    console.log(this.state.editorState);
  };
  onContentStateChange: Function = contentState => {
    this.setState({
      contentState
    });
  };
  render() {
    // const sampleMarkup =
    // '<b>Bold text</b>, <i>Italic text</i><br/ ><br />' +
    // '<a href="http://www.facebook.com">Example link</a>';
    // const blocksFromHTML = convertFromHTML(sampleMarkup);
    // const state = ContentState.createFromBlockArray(
    //   blocksFromHTML.contentBlocks,
    //   blocksFromHTML.entityMap
    // );

    const { editorState, contentState } = this.state;
    return (
      <div>
        <Modal visible={false} style={{ top: 30 }}>
          <Editor
            editorState={editorState}
            // contentState={contentState}
            wrapperClassName="demo-wrapper"
            editorClassName="demo-editor"
            onEditorStateChange={this.onEditorStateChange}
            // onContentStateChange={this.onContentStateChange}
          />

          <textarea disabled value={draftToHtml(convertToRaw(editorState.getCurrentContent()))} />
          <textarea disabled value={JSON.stringify(contentState, null, 4)} />
        </Modal>
      </div>
    );
  }
}
