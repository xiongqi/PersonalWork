import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { observable, action, autorun, computed, useStrict } from 'mobx';
import { observer } from 'mobx-react';
import User from './components/User';

useStrict(true);

let appState = observable({
  timer: 0,
  second: 10
});
appState.resetTimer = action(function() {
  appState.timer += 1;
});
const unAutorun = autorun(() => {
  appState.timer;
  console.log('second', appState.second);
});

@observer
class App extends React.Component {
  onReset = () => {
    this.props.appState.resetTimer();
  };
  render() {
    return (
      <div>
        <button onClick={this.onReset}>{this.props.appState.timer}</button>
        <User appState={appState}/>
      </div>
    );
  }
}

const main = document.getElementById('main');
ReactDOM.render(<App appState={appState} />, main);
