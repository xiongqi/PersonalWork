var ExtractTextPlugin=require("extract-text-webpack-plugin");

module.exports={
    entry:__dirname+"/app/main.js",
    output:{
        path:__dirname+"/build",
        filename:"bundle.js"
    },
    devtool:"eval-source-map",
    module:{
        loaders:[
            {
                test:/\.(js|jsx)$/,
                exclude:/node_modules/,
                loader:'babel-loader'
            },
            {
                test:/\.(png|jpg|gif)$/,
                loader:'url-loader?limit=8192&name=images/[name].[ext]'
            },
            {
                test:/\.css$/,
                loader:"style-loader!css-loader"
                //loader:ExtractTextPlugin.extract("style-loader","css-loader")
            }
        ]
    },
    devServer:{
        contentBase:'./build',
        historyApiFallback:true,
        inline:true,
        port:8080
    },
    plugins:[
        new ExtractTextPlugin("styles.css")
    ]
};