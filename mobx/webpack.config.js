
module.exports = {
  entry: './src/app.js',
  output: {
    filename: 'bundle.js', // 打包文件名
    path: './build', // webpack本地打包路径,于publicPath作用不同

    // 运行服务器时的打包文件夹路径,即打包在 "http://网站根目录/dist/"下,通过"http://网站根目录/dist/bundle.js"访问.
    publicPath: '/dist/' // http://www.jb51.net/article/116443.htm  publicPath路径问题详解
  },
  devtool: 'eval-source-map',
  devServer: {
    contentBase: './public', // webpack-dev-server提供本地服务器的文件夹
    inline: true, // 热更新
    historyApiFallback: true, // false 根据路径跳转页面 ; true 路径都返回根index.html
    port: 8089
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          { loader: 'css-loader' },
          {
            loader: 'postcss-loader',
            options: {
              plugins: loader => [
                require('autoprefixer')(/* {browsers: ['last 5 versions']} */)
              ]
            }
          }
        ]
      }
    ]
  }
};
