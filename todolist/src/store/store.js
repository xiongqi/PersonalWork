import { applyMiddleware, createStore } from 'redux';
import rootReducer from '../reducers/rootReducers';
import { createLogger } from 'redux-logger';

const logger = createLogger({
  collapsed:true
});
const store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(logger),
);
// store.subscribe(() => console.log('store.state:', store.getState()));
export default store;
