reason:
this.state = {
  platformList: [], // 平台
  fields: [], // 用户输入的信息
  data: [], // 列表信息
  editRow: null, // 新建项的信息
}
平台:{data: [{id: 1, supNm: "点我吧", sorting: 1}, {id: 2, supNm: "点点送", sorting: 99999}], success: true}
列表:{"data":[{"id":1,"channel":2,"channelName":"ertgre","adjustReason":"122222","status":1,"creator":"系统管理员","modifier":"df","createTime":"2017-11-06T02:40:16.000Z","modifyTime":"2017-11-10T07:52:09.000Z"},,,],"success":true}

1.Upload Table 渲染 (antdesign)
2.onSubmitOne 提交数据,写入数据库
<SearchBar
  onSubmit={this.onSearch}
  fields={this.searchFields()}
/>
1.componentWillMount{this.setDefaultValue();}
||
2.setDefaultValue():this.props.fields 判断三个选项默认值
||
3.有默认值的项(即状态),设置默认值:this.setField(field, field.defaultValue); // 设置 状态
||
4.render:{this.generateInputs(this.props.fields)} 渲染三个选项
||
5.判断 reset 按钮
||
6.componentDidMount  // 清空style??datetime专用
||
7.搜索按钮:onClick={this.handleSubmit} 判断warning,整理用户输入的fields调用父组件函数
||
8.this.props.onSubmit(fields); 重设fields,再次查询后台,过滤=>loadTableList() 添加ajax参数

this.state = {
  fields: session || {}, // 存储用户操作的后的三个选项的值
  autoComplete: {},
  disabled: {},
  warnings: {},
};
this.props.fields = [
  {
    title: '渠道',
    key: 'channel',
    type: 'select',
    showSearch: true,
    filterOption: (inputValue, option) => option.props.children.toLowerCase().indexOf(inputValue.toLowerCase()) >= 0, // inputvalue在options中
    items: () => this.state.platformList.map(d => ({ value: String(d.id), mean: d.supNm })) // 平台信息
  },
  {
    title: '调整原因',
    key: 'adjustReason',
    type: 'input',
  },
  {
    title: '状态',
    key: 'status',
    type: 'select',
    defaultValue: '',
    items: () => [{ value: '', mean: '全部' }, { value: '1', mean: '生效' }, { value: '0', mean: '失效' }],
  }
]
onSearch = (fields) => {
  this.setState({ fields }, () => {
    this.loadTableList()
  })
}