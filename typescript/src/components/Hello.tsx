import * as React from 'react';
import './Hello.css';
import * as $ from 'jquery';

interface Props {
  name: string;
  level?: number; // ?表示可选的
}
function getLevel(level: number) {
  if (level < 1) {
    return '😢';
  }
  return Array(level + 1).join('!'); // 66666666666
}

export default function Hello ( { name, level= 1 }: Props ) {
  return (
    <div className="hello">
      Hello {name + ' ' + getLevel(level)}
    </div>
  );
}

export class HelloClass extends React.Component<Props> { // React.Component<props的类型>
  render() {
    const {name, level= 1} = this.props;
    return (
    <div className="hello">
      Hello {name + ' ' + getLevel(level)}
    </div>
    );
  }
}
/* 
declare namespace React {
  
    // Base component for plain JS classes
    // tslint:disable-next-line:no-empty-interface
    interface Component<P = {}, S = {}> extends ComponentLifecycle<P, S> { }
    class Component<P, S> {
        constructor(props: P, context?: any);

        // Disabling unified-signatures to have separate overloads. It's easier to understand this way.
        // tslint:disable-next-line:unified-signatures
        setState<K extends keyof S>(f: (prevState: Readonly<S>, props: P) => Pick<S, K>, callback?: () => any): void;
        // tslint:disable-next-line:unified-signatures
        setState<K extends keyof S>(state: Pick<S, K>, callback?: () => any): void;

        forceUpdate(callBack?: () => any): void;
        render(): ReactNode;

        // React.Props<T> is now deprecated, which means that the `children`
        // property is not available on `P` by default, even though you can
        // always pass children as variadic arguments to `createElement`.
        // In the future, if we can define its call signature conditionally
        // on the existence of `children` in `P`, then we should remove this.
        props: Readonly<{ children?: ReactNode }> & Readonly<P>;
        state: Readonly<S>;
        context: any;
        refs: {
            [key: string]: ReactInstance
        };
    }
} 
*/