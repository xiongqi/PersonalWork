[JSON schema for TS configuration file](http://json.schemastore.org/tsconfig)  
1. `tsconfig.json`文件存在于项目根目录，指定了用来编译这个项目的根文件和编译选项。若为空文件,所有选项采用默认配置.
    1. 不带任何输入文件的情况下调用tsc，编译器会从当前目录开始去查找tsconfig.json文件，逐级向上搜索父目录。  
    2. 不带任何输入文件的情况下调用tsc，且使用命令行参数--project（或-p）指定一个包含tsconfig.json文件的目录。  
    3. 当命令行上指定了输入文件时，tsconfig.json文件会被忽略。  
2. `typescript`采用`glob`匹配模式：
* `*` 匹配0或多个字符 （不含目录分隔符）
* `?` 匹配一个任意字符（不含目录分割符）
* `**/` 递归匹配任意子目录
3. 使用`include`引入的文件可以使用`exclude`属性过滤.通过`files`属性明确指定的文件却总是会被包含在内,不管`exclude`如何设置.
4. 编译器不会引入可能作为输出的文件;假如包含了`index.ts`,那么`index.d.ts`和`index.js`会被排除在外.不推荐只有扩展名的不同来区分同目录下的文件.
5. `'files'`: If no `files` or `include` property is present in a tsconfig.json, the compiler defaults to including all files in the containing directory and subdirectories except those specified by `exclude`.
    1. `fiels`和`include`共同指定要编译的文件,`exclude`会忽略`include`指定的相应文件,不会影响`files`.
    2. `rootDir`用来控制输出目录的结构.
