import { combineReducers } from 'redux';
import { SET_FILTER, FilterOptions, ADD, COMPLETED_CHANGE, TOGGLE } from '../action/action';
// import axios from 'axios';
// import { fetchJSON, axiosPatchJSON } from '../apis/jserver';

function filterReducer(visibleTodoFilter = FilterOptions.SHOW_ALL, action) {
  // state.visibleTodoFilter
  switch (action.type) {
    case SET_FILTER:
      return action.filter;
    default:
      return visibleTodoFilter;
  }
}

function todosReducer(todos = [], action) {
  switch (action.type) {
    case ADD:        
      return action.text;
    case COMPLETED_CHANGE:      
      return action.data;
    case 'INITIAL':
      return action.data;
    default:
      return todos;
  }
}
/* function todosReducer(todos = [], action) {
  if(todos.length == 0){
    fetchJSON('/api/data').then(data=>{todos = data;debugger});
  }
  // console.log(typeof todos);
  // debugger
  switch (action.type) {
    case ADD:  
      todos = axiosPatchJSON('/api/data', [...todos, { completed: false, text: action.text, key: +new Date() }]);
      return todos;
    case COMPLETED_CHANGE:
      todos = todos.map(todo => {
        if (todo.key == action.key) return Object.assign({}, todo, { completed: !todo.completed });
        return todo;
      });
      todos = axiosPatchJSON('/api/data', todos);
      return todos;
    case 'INITIAL':
      return action.data;
    default:
      return todos;
  }
} */
function toggleReducer(toggle = false, action) {
  switch (action.type) {
    case TOGGLE:
      return toggle = !action.toggle;
    default:
      return toggle;
  }
}
const rootReducer = combineReducers({
  // state.* : reducer
  visibleTodoFilter: filterReducer,
  todos: todosReducer,
  toggle: toggleReducer
});
export default rootReducer;
