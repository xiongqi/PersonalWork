import React, { Component } from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
require("./image.css");
require("./test.js");

var name = "XQ";
var jsxElement = <h2>{name}: 这是jsx element</h2>;
ReactDOM.render(
  <div>
    <h1>Hello World {name}</h1>
    {jsxElement}
  </div>,
  document.getElementById("root")
);

class ReactApp extends Component {
  constructor(props) {
    super(props);
    this.state = { count: 0, count2: 10 };
  }
  componentDidMount() {
    this.interval = setInterval(() => {
      //   this.setState({ count: this.state.count + 1 });
      this.setState(pre => {
        this.state.count= pre.count + 1;//有效
        return ({count:pre.count + 1});//也有效
      });/* 加()是return，不加是语句，错误 */
      if (this.state.count < 100) {        
        this.forceUpdate(); //console.log(this.state.count);
      }
      else clearInterval(this.interval);
    }, 1000);
  }
  componentWillUnmount(){
    clearInterval(this.interval);
  }
  render() {
    return (
      <div>
        <h2>time:{new Date().toLocaleString()}</h2>
      </div>
    );
  }
}
function ReactComp (props){
  return (
  <div>{props.defaultTrue?props.func:"false"}
    {props.defaultTrue&&<div>显示 </div>} 
    {/* {new Error(
        'Invalid prop `' + props.func + '` supplied to' +
        ' `' + props.func + '`. Validation failed.'
      )} */}
  </div>);
}
ReactComp.propTypes={
  func:PropTypes.string
};
ReactDOM.render(<ReactComp defaultTrue={1} func={"&lt;3"}/>, document.getElementById("root3"));
ReactDOM.render(<ReactApp />, document.getElementById("root2"));
