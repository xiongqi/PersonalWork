import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Input, Button } from 'antd';

const InputConf = {
  placeholder: '代办事项',
  type: 'tetx'
};
const ButtonConf = {
  type: 'primary'
};
const RowConf = {
  type: 'flex',
  justify: 'center',
  // gutter: 10,
  style: { textAlign: 'center'},
};
export default class AddTodo extends React.Component {
  render() {
    return (
      <Row {...RowConf}>
        <Col span={6}>
          <Input {...InputConf} ref="input" />
        </Col>
        <Col span={2}>
          <Button {...ButtonConf} onClick={() => this.handleClick()}>
            Add
          </Button>
        </Col>
      </Row>
    );
  }
  handleClick() {
    const input = this.refs.input.refs.input; // ????
    const text = input.value.trim();
    if (text !== '')
      this.props.onAddClick(text); // 调用父组件传入的函数
    input.value = ''; // 清空输入
  }
}
AddTodo.PropTypes = {
  onAddClick: PropTypes.func.isRequired // 点击函数,必须
};
