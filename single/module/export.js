/**
 * 模块：自动运行在严格模式下的并且无法退出的JavaScript代码
 * 1.自动运行，严格模式
 * 2.模块顶部 this 是 undefined
 * 3.模块不支持 HTML 风格注释
 * 4.脚本：任何不是模块的 JavaScript 代码，缺少模块特性
 * 5.import导入绑定时，好像使用const定义一样，无法再定义同名变量或者重赋值
**/

export let color="red";
export function sum(a,b){
  return a+b;
}
 class circle{
  constructor(r){
    this.r=r;
  }
}
function mumlll(a,b){
  return a*b;
}
export {mumlll,circle};