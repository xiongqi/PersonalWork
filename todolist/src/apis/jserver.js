import axios from 'axios';

export async function fetchJSON2(url, params = []) {
  let myHeaders = new Headers();
  // myHeaders.append('Content-Type', 'application/json');
  params = {
    // ...params,
    method: 'GET',
    credentials: 'include',
    headers: myHeaders,
  };
  let fetchResponse, json;
  await fetch(url, params)
    .then((res) => {
      console.log('fetchJSON:Response:', res);
      fetchResponse = res;
      return res.json();
    })
    .then(data => (json = data));
  if (fetchResponse.status == 200) {
    console.log(json); // object
    return json.todoList;
  }
  return [];
}
export function fetchJSON(url, params = []) {
  let myHeaders = new Headers();
  // myHeaders.append('Content-Type', 'application/json');
  params = {
    // ...params,
    method: 'GET',
    credentials: 'include',
    headers: myHeaders,
  };
  return fetch(url, params);
}

export async function axiosPatchJSON2(url, data = []) {
  const params = {
    method: 'PATCH',
    headers: { 'Content-Type': 'application/json' },
    data: JSON.stringify({ todoList: data }),
  };
  let axiosResponse;
  await axios(url, params).then(res => {
    axiosResponse = res;
  });
  console.log('axiosPatchJSON:Response:', axiosResponse);
  if (axiosResponse.status == 200) return JSON.parse(axiosResponse.data.todoList);
  return data;
}
export function axiosPatchJSON(url, data = []) {
  const params = {
    method: 'PATCH',
    headers: { 'Content-Type': 'application/json' },
    data: JSON.stringify({ todoList: data }),
  };
  return axios(url, params);
}
