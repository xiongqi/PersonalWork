export const ADD = 'ADD';
export const COMPLETED_CHANGE = 'COMPLETED_CHANGE';
export const SET_FILTER = 'SET_FILTER';
export const TOGGLE = 'TOGGLE';

export const FilterOptions = {
  SHOW_ALL: 'SHOW_ALL',
  SHOW_COMPLETED: 'SHOW_COMPLETED',
  SHOW_ACTIVE: 'SHOW_ACTIVE'
};

export function initial(data) {
  return { type: 'INITIAL', data};
}
export function addTodo(text) {
  return { type: ADD, text };
}
export function completeChange(data) {
  return { type: COMPLETED_CHANGE, data };
}
export function setFilter(filter) {
  return { type: SET_FILTER, filter };
}
export function setToggle(toggle) {
  return { type: TOGGLE, toggle };
}

// export { ADD, COMPLETED_CHANGE, SET_FILTER, TOGGLE, FilterOptions, addTodo, completeChange, setFilter, setToggle };
