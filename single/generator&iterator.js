// yield 执行情况
function* generator1() {
  console.log(1);
  yield 1;
  console.log(2);
  yield 2;
  console.log(3);
  return 3;
}
let iterator1 = generator1();
console.log(iterator1.next()); // 1 {value:1,done:false}
console.log(iterator1.next()); // 2 {value:2,done:false}
console.log(iterator1.next()); // 3 {value:3,done:false}
console.log(iterator1.next()); // {value:undefined,done:false}

// string for-of
let str = 'abc';
for (let i of str) console.log(i); // a b c
for (let i in str) console.log(i); // 0 1 2

// 写法
let o = {
  generator1: function*() {},
  *generator2() {},
};
o;

// 迭代对象 给Symbol.iterator属性添加迭代器
console.log(iterator1[Symbol.iterator]);
let collection = {
  items: ['a', 'b', 'c'],
  *[Symbol.iterator]() {
    for (let i of this.items) {
      yield i + 1;
    }
  },
};
for (let i of collection) {
  console.log(i); // a1 b1 c1
}

// 迭代器传参
/**
 * 1.第一次调用next()无论传入什么参数都会被丢弃.
 * 2.调用.throw()会使迭代器继续执行(next()).
 */
function* createIterator() {
  let a = yield 1;
  let b ;
  try {
  b = yield a + 1;
  } catch(e){
    b = 100;
  }
  yield b + 1;
}
let iterator2 = createIterator();
console.log(iterator2.next());
console.log(iterator2.next(4));
console.log(iterator2.throw(new Error('error')));
console.log(iterator2.next());

// 委托生成器 yield语句添加 * 号
console.log('委托生成器');
function* g1(){
  yield 1;
  yield 2;
  return 3; // 无效,存在于生成器内部
}
function* g(){
  let result = yield *g1(); // result 值为正常函数返回值(return),没有return为undefined.
  console.log(result);  
  yield true;
  yield 'str';
  yield *'str';
  return 'ok';
}
let t = g();
for(let i=0;i<10;i++)
  console.log(t.next());console.log();
  
console.log(Object.prototype.toString.call(g1)); // [object GeneratorFunction]
