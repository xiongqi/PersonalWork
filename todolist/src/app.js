import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, browserHistory, IndexRoute, Redirect } from 'react-router';
import TodoAppContainer from './container/TodoApp';
import store from './store/store';
import ToDoListContainer from './components/TodoList';
import { VisualContainer } from './components/Visual';
// const test =require('./index.js');

const main = document.getElementById('main');
console.log(4);

ReactDOM.render(
  <Provider store={store}>
    {/* <TodoAppContainer/> */}
    <Router history={browserHistory}>
      <Route path="/" component={TodoAppContainer}>
        <IndexRoute onEnter={console.log('Welcome to TodoList')} component={ToDoListContainer} />
        <Redirect from="table" to="/" />
        <Route path="/visual" component={VisualContainer} />
      </Route>
    </Router>
  </Provider>,
  main
);
