import React from 'react';
import LzEditor from 'react-lz-editor';
import { Modal, Button } from 'antd';

export default class Editor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      htmlContent: '',
      editorData: ''
    };
    this.receiveHtml = this.receiveHtml.bind(this);
  }
  receiveHtml(content) {
    console.log(content);
    this.setState({ editorData: content });
  }
  componentWillMount() {
    // this.setState();
  }
  handleContent = () => {
    console.log(this.handlePlainText());
    console.log(this.handlePlainText2());
  };
  handlePlainText = () => {
    const blockTag = {
      address: 1,
      blockquote: 1,
      center: 1,
      dir: 1,
      div: 1,
      dl: 1,
      fieldset: 1,
      form: 1,
      h1: 1,
      h2: 1,
      h3: 1,
      h4: 1,
      h5: 1,
      h6: 1,
      hr: 1,
      isindex: 1,
      menu: 1,
      noframes: 1,
      ol: 1,
      p: 1,
      pre: 1,
      table: 1,
      ul: 1,
      ADDRESS: 1,
      BLOCKQUOTE: 1,
      CENTER: 1,
      DIR: 1,
      DIV: 1,
      DL: 1,
      FIELDSET: 1,
      FORM: 1,
      H1: 1,
      H2: 1,
      H3: 1,
      H4: 1,
      H5: 1,
      H6: 1,
      HR: 1,
      ISINDEX: 1,
      MENU: 1,
      NOFRAMES: 1,
      OL: 1,
      P: 1,
      PRE: 1,
      TABLE: 1,
      UL: 1
    };
    const data = this.state.editorData;
    let html = data.replace(/[\n\r]/g, ''); // ie要先去了\n在处理
    html = html
      .replace(/<(p|div)[^>]*>(<br\/?>|&nbsp;)<\/\1>/gi, '\n')
      .replace(/<br\/?>/gi, '\n')
      .replace(/<[^>/]+>/g, '')
      .replace(/(\n)?<\/([^>]+)>/g, function(match, s1, s2) {
        return blockTag[s2] ? '\n' : s1 ? s1 : '';
      });
    // 取出来的空格会有c2a0会变成乱码，处理这种情况\u00a0
    return html.replace(/\u00a0/g, ' ').replace(/&nbsp;/g, ' ');
  };
  handlePlainText2 = () => {
    const blockTag = {
      address: 1,
      blockquote: 1,
      center: 1,
      dir: 1,
      div: 1,
      dl: 1,
      fieldset: 1,
      form: 1,
      h1: 1,
      h2: 1,
      h3: 1,
      h4: 1,
      h5: 1,
      h6: 1,
      hr: 1,
      isindex: 1,
      menu: 1,
      noframes: 1,
      ol: 1,
      p: 1,
      pre: 1,
      table: 1,
      ul: 1,
      ADDRESS: 1,
      BLOCKQUOTE: 1,
      CENTER: 1,
      DIR: 1,
      DIV: 1,
      DL: 1,
      FIELDSET: 1,
      FORM: 1,
      H1: 1,
      H2: 1,
      H3: 1,
      H4: 1,
      H5: 1,
      H6: 1,
      HR: 1,
      ISINDEX: 1,
      MENU: 1,
      NOFRAMES: 1,
      OL: 1,
      P: 1,
      PRE: 1,
      TABLE: 1,
      UL: 1
    };
    const data = this.state.editorData;
    let element = document.createElement('div');
    element.innerHTML = data;
    const ele = element.children;
    let str = '';
    for (let i of ele) {
      str += i.innerText;
      if (blockTag[i.tagName] && i.innerText != '\n') {
        
        str += '\n';
      }

      // blockTag[i.tagName] == 1;
      // debugger
    }
    return str;
  };
  render() {
    // const data = this.props.data;
    const config = {
      image: false,
      video: false,
      audio: false,
      autoSave: false,
      active: false,
      removeStyle: false,
      pasteNoStyle: false,
      color: false,
      importContent: this.state.htmlContent,
      cbReceiver: this.receiveHtml
    };
    const modal = {
      visible: true,
      style: { top: 30 }
    };
    return (
      <Modal {...modal}>
        <Button onClick={this.handleContent}>content</Button>
        <LzEditor {...config} />
      </Modal>
    );
  }
}
