[阮一峰——Redux 入门教程（三）：React-Redux 的用法](http://www.ruanyifeng.com/blog/2016/09/redux_tutorial_part_three_react-redux.html)
```js
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import { createStore } from 'redux'
import { Provider, connect } from 'react-redux'

// React component
class Counter extends Component {
  render() { // 使用this.props:{value,onIncreaseClick}
    const { value, onIncreaseClick } = this.props
    return (
      <div>
        <span>{value}</span>
        <button onClick={onIncreaseClick}>Increase</button>
      </div>
    )
  }
}

Counter.propTypes = { // 校验
  value: PropTypes.number.isRequired,
  onIncreaseClick: PropTypes.func.isRequired
}

// Action
const increaseAction = { type: 'increase' }

// Reducer
function counter(state = { count: 0 }, action) {
  const count = state.count
  switch (action.type) {
    case 'increase':
      return { count: count + 1 }
    default:
      return state
  }
}

// Store 
const store = createStore(counter)

/**
 * Counter props : { value,onIncreaseClick } 
 **/ 
// Map Redux state to component props
function mapStateToProps(state) { // 输入逻辑：外部的数据（即state对象）如何转换为 UI 组件的参数
  return {
    value: state.count // props:state
  }
}
// Map Redux actions to component props
function mapDispatchToProps(dispatch) { // 输出逻辑：用户发出的动作如何变为 Action 对象，从 UI 组件传出去。
  return { // props:dispatch
    onIncreaseClick: () => dispatch(increaseAction)
  }
}

// Connected Component
const App = connect(
  mapStateToProps,
  mapDispatchToProps
)(Counter) // Counter 即 UI 组件

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)
```

```js
1.class Counter 使用 this.props
2.定义action，reducer，store
3.数据格式:
Object { 
  visibleTodoFilter:"SHOW_ALL"(SHOW_COMPLETED/SHOW_ACTIVE),
  todos:[{},{},{
    completed:true/false,
    text:""
  }]
}
```