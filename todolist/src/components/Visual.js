import echarts from 'echarts';
import React from 'react';
import { connect } from 'react-redux';

export default class Visual extends React.Component {
  componentDidMount() {
    this.setOption();
  }
  componentDidUpdate() {
    this.setOption();
  }
  tipFunc = params => {
    if (typeof params.value === 'string') return params.value;
    const value = params.value;
    const thing = `<b>${value.text}</b><br/>`;
    const date = `<small>${new Date(value.key).toLocaleString()}</small><br/>`;
    const isDone = `<small style="color:${value.completed ? 'green">已完成' : 'red">未完成'}</small>`;
    return `<div>${thing + date + isDone}</div>`;
  };
  setOption = (did = [], doing = []) => {
    const myChart = echarts.init(document.getElementById('visual'));
    this.props.todos.forEach(todo => {
      const c = todo.completed;
      const color = c ? 'green' : 'red';
      const node = { name: todo.text, value: todo, label: { normal: { color: color } }, itemStyle: { normal: { borderColor: color, color: color } } };
      if (todo.completed) did.push(Object.assign({}, todo, node));
      else doing.push(Object.assign({}, todo, node));
    }); // forEach 无返回值
    const firstChildren = [
      { name: '未做', value: '未做', label: { normal: { color: 'red' } }, itemStyle: { normal: { borderColor: 'red', color: 'red' } }, children: doing },
      { name: '已做', value: '已做', label: { normal: { color: 'green' } }, itemStyle: { normal: { borderColor: 'green', color: 'green' } }, children: did }
    ];
    const option = {
      // 绘制
      title: { text: 'All List Vis' },
      tooltip: { trigger: 'item', triggerOn: 'mousemove', backgroundColor: '#f5f5d5', textStyle: { color: 'grey' } },
      series: [
        {
          name: 'visual',
          type: 'tree',
          left: '15%',
          right: '30%',
          top: '0%',
          tooltip: { formatter: this.tipFunc },
          itemStyle: { normal: { borderColor: 'blue', color: 'blue' } },
          data: [{ name: 'List', value: 'TodoList', children: firstChildren }],
          label: { normal: { position: 'top', color: 'blue' } },
          leaves: { label: { normal: { position: 'right' } } },
          initialTreeDepth: 3,
          symbolSize: 7,
          symbol:'emptyCircle'
        }
      ]
    };
    // debugger
    myChart.setOption(option);
  };
  render() {
    return <div id="visual" style={{ width: '600px', height: '300px', margin: '0 auto' }} />;
  }
}

const VisualContainer = connect(state => ({ todos: state.todos }))(Visual);
export { VisualContainer };
