import React from 'react';
import axios from 'axios';
import { userApi, searchApi } from '../apis/githubApi';

export default class User extends React.Component {
  constructor() {
    super();
    this.state = {
      userName: '',
      userId: 0,
      userBio: '',
      serverData: [],
    };
  }
  componentDidMount() {
    axios
      .get(userApi)
      .then(res => {
        console.log(res);
        this.setState({ userName: res.data.login });
      })
      .then(() => {
        axios({
          method: 'get',
          url: searchApi,
          params: {
            q: 'xqone',
          },
        }).then(res => {
          // console.log(res);debugger
          res.data.items.forEach(x => {
            if (x.login === 'XQONE') this.setState({ userId: x.id });
          });
        });
      });

    fetch(userApi)
      .then(res =>
        // console.log(res.json());
        res.json() // .text() // Body mixins,The Body functions can be run only once; next calls will resolve with empty strings/ArrayBuffers.
      )
      .then(res => {
        // Fetch API 中的 Body mixin 代表 代表响应/请求的正文，允许你声明其内容类型是什么以及应该如何处理。
        console.log(res);
        this.setState({ userBio: res.bio });
        // debugger
      });

    // axios.get('http://localhost/').then(res => {
    //   console.log(res);
    //   this.setState({ serverData: res.data.todos });
    //   // this.setState({ serverData: res.data.todos[0].text });
    // })
    // .catch(err => {
    //   console.log(err)
    // });
  }
  change = () => {
    // const option = {params:{name:'xq2'}};
    axios.get('https://devdwbbucket.oss-cn-hangzhou.aliyuncs.com/avm-local/spider_3.0.1_test.json').then(
      res => {
        alert(typeof res.data === 'object');
      },
      rej => console.log(rej)
    );
  };
  render() {
    return (
      <div style={{ position: 'fixed' }}>
        UserName：{this.state.userName} <br />
        UserId：{this.state.userId} <br />
        UserBio：{this.state.userBio} <br />
        {/* ServerData：{this.state.serverData.length} <br /> */}
        <button onClick={this.change}>change?</button>
      </div>
    );
  }
}
