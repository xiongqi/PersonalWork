// 类型注解 type annotations
function greeter(person: string) {
  return 'Hello ' + person;
}
let user = 'XQ';
greeter(user);

// interface
function greeter2(person: Person) {
  return person.name
  // return person.sex; // 错误提示
}
interface Person {
  readonly name: string;
  age?: number;
}
let user2 = { name: 'XQ2', age: 20, sex:'male' };
greeter2(user2);

// class
class Student {
  name: string;
  constructor(public age: number) { // public 创建了同名变量 this.age=age
    
  }
}
interface Person3 {
  name: string;
  age: number;
}
function greeter3(person: Person3) {
  return person.name;
}
let user3 = new Student(30);
greeter3(user3);

// Tuple 元组
let x: [string, number] = ['sd', 2];
let y: [string, number] = ['sd', 2, 4]; // 越界时,使用联合类型替代
// x[1].substr(1); // hao

// 枚举
enum Color {Red = 3, Green = 's', Blue = 5} // Color[Color["Red"] = 3] = "Red"
let c: Color = Color.Green;
console.log(c); // '4'

type c = {a:string,b?:number};
function f({a,b}:c):void{ }

function f2({a,b=0}={a:''}):void{} // {a:string,b:numer} | undefined
f2({a:'yes'});
f2()
// f2({}) error

let ro: ReadonlyArray<number> = [1,2,3,4]
// ro[0]=1;// error
let a = ro 

// 函数类型
interface SearchFunc {
  (source:string, sub:string): boolean //  它就像是一个只有参数列表和返回值类型的函数定义。参数列表里的每个参数都需要名字和类型。
}
let mySearch: SearchFunc;
// 函数的参数会逐个进行检查，要求对应位置上的参数类型是兼容的。
mySearch = function(s:string,sub){
  return true
}
mySearch('a','c')

//索引签名
interface  StringArray {
  [index:number]: string
}
let myArray:StringArray = ['x','q']

// 类类型 接口描述了类的公共部分
interface ClockInterface {
  currentTime: Date;
  setTime(d:Date):void;
}
class Clock implements ClockInterface{
  currentTime: Date;
  setTime(d:Date){
    this.currentTime = d;
  }
}

// 继承借口
interface Shape{
  color: string
}
interface Pen{
  Width:number
}
interface Square extends Shape, Pen{
  side: number
}
let square = <Square>{}
square.Width=100
// square.height=100 // error

// 接口继承类
class Parent{
  private state:any
}
interface Selectable extends Parent{
  select():void
}
// 因为 state是私有成员，所以只能够是Parent的子类们才能实现Selectable接口
class Child extends Parent implements Selectable{
  select(){}
}
// 错误：“Image”类型缺少“state”属性。
// class Fake implements Selectable {
//   select() { }
// }

// 函数类型  (x:number,y:number)=>number 是类型申明 函数类型包含两部分：参数类型和返回值类型
let myAdd:(x:number,y:number)=>number = function(xx:number,yy:number){return xx+yy}

// 泛型
  /* 写法1 */
function identity<T>(arg:T):T{
  return arg
}
let myIdentity2 : <U>(a:U)=>U = identity
  /* 写法2 */
interface Identity{
  <T>(a:T):T
}
let myIdentity3: Identity = identity
let myIdentity : {<T>(a:T):T} = identity /* 写法2简写 */

// 泛型约束 含 length 属性的任意类型T
function logging<T extends Length>(a:T):number{
  return a.length;
}
interface Length {
  length:number
}

// 泛型里使用类类型  参数c是构造函数,T是实例类型
function create<T>(c: {new (): T; }): T {
  return new c();
}
 /*  */
class BeeKeeper {
  hasMask: boolean;
}
class ZooKeeper {
  nametag: string;
}
class Animal {
  numLegs: number;
}
class Bee extends Animal {
  keeper: BeeKeeper;
}
class Lion extends Animal {
  keeper: ZooKeeper;
}
function createInstance<A extends Animal>(c: new () => A): A {
  return new c(); // A是继承了Animal的类的实例  c是A实例的类的构造函数性质的类
}
createInstance(Lion).keeper.nametag;  // typechecks!
createInstance(Bee).keeper.hasMask;   // typechecks!

// 构造函数
class Greeter {
  static standardGreeting = "Hello, there";
  greeting: string;
  greet() {
      if (this.greeting) {
          return "Hello, " + this.greeting;
      }
      else {
          return Greeter.standardGreeting;
      }
  }
}
let greeter1: Greeter;
greeter1 = new Greeter();
console.log(greeter1.greet());

let greeterMaker: typeof Greeter = Greeter;
greeterMaker.standardGreeting = "Hey there!";
 
// 类型别名
type str = string
type func = (a: str, b: number) => boolean
const test1: func = function(a){
  return true
}
test1('str',1)