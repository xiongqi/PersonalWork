import React from 'react';
import PropTypes from 'prop-types';

export default class Todo extends React.Component {
  render() {
    return (
      <a onClick={this.props.onClick} style={{ textDecoration: this.props.todo.completed ? 'line-through' : 'none' }}>
        {this.props.todo.text}
      </a>
    );
  }
}
Todo.PropTypes = {
  todo: PropTypes.object
};
