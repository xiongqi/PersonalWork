// 解构：使数据访问更便捷;右侧被解构参数为 null，undefined 回抛出错误
let node = {
  type: 'Identifier',
  name: 'foo',
  third: 'third',
  loc: {
    start: {
      line: 1
    },
    end: {
      line: 2
    }
  }
};
let { type, name } = node;
console.log(type, name);

// 解构赋值声明变量必须进行初始化，否则错误
// var {some};let {some};const {some} //error

({ type, name } = node); // 已定义变量赋值需加小括号转为表达式
let { value1, value2 = true } = node; // undefined,true ; 不存在的值赋值为 undefined或自带的默认值

// 不同命变量赋值,type,value,third并不会存在于环境中。
let { type: localType, value: localValue = false, third: localThird } = node;

// 嵌套解构 , 写法类似对象结构
let { loc: { start } } = node; // {line:1}
let { loc: { start: localStart } } = node; // {line:1}
let { loc: { start: { line } } } = node; // 1

// 数组解构
let colors = ['red', 'green', 'yellow'];
let firstColor = 'first';
[firstColor] = colors; // "red"
let [, , thirdColor, forthColor = 'blue', fifthColor] = colors; // "yellow" "blue" undefined

// 交换变量值
[firstColor, thirdColor] = [thirdColor, firstColor];
[node.type] = [firstColor];

// 不定解构,可用作复制数组:colors.concat()
let [...rest] = colors; // ["red","green","yellow"]

