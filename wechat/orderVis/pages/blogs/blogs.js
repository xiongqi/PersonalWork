//logs.js
const util = require('../../utils/util.js')

Page({
  data: {
    logs: []
  },
  onLoad: function () {
    // this.setData({
    //   logs: (wx.getStorageSync('logs') || []).map(log => {
    //     return util.formatTime(new Date(log))
    //   })
    // })
    // wx.navigateBack({delta:3})
    const that = this
    wx.request({
      url: 'https://xhuyq.me/resources/%E5%8D%9A%E6%96%87%E5%9C%B0%E5%9D%80.txt',
      success: function (res) {
        const data = res.data.split('\n');
        const d2 = data.map(x => {
          const name = x.substring(x.indexOf('[') + 1, x.indexOf(']'));
          const url = x.substring(x.indexOf('(') + 1, x.indexOf(')')).replace("http://xqone.github.io/","https://xhuyq.me/");
          return { name, url }
        })
        console.log(d2)
        that.setData({
          logs: d2
        })
      }
    })
    console.log(wx.createSelectorQuery().selectAll('#v'))
  }
})
