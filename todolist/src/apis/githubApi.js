const api = 'https://api.github.com';
const user = '/users/xqone';
const search = '/search/users';

const userApi = api + user;
const searchApi = api + search;
export { userApi, searchApi };
