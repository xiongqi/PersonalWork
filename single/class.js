// es5
function PersonType(name) {
  // 1.创建构造函数
  this.name = name;
  let sex = 'male'; // 不会被实例化
}
PersonType.prototype.sayName = function() {
  // 2.赋值给构造函数原型
  console.log(this.name);
};
let person = new PersonType('xq');
person.sayName();

// es6 类申明 语法糖,实质是一个具有构造函数方法行为的函数
let methodName = 'xq';
class PersonClass { // let PersonClass=class {}  
  constructor(name) { // 等价于PersonType构造函数,除constructor外没有其他保留方法名    
    this.name = name;
  }
  sayName() {
    // 等价于PersonType.prototype.sayName
    console.log(this.name, typeof PersonClass);
  }
  [methodName]() {
    // 可计算名称
    console.log('nothing');
  }

  /**
   * 1.等价于 PersonType.create , 静态方法
   * 2.不可在实例中访问静态方法,必须直接在类中访问
   * 3.不能用 statc 定义构造函数
   */

  static create(name) {
    return new PersonClass(name);
  }
}
let personc = new PersonClass('xq-c');
let personc2 = PersonClass.create('xq2-c');
personc.sayName(); // "xq2" "function"
personc.xq();

// 继承
class Rectangle {
  constructor(length, width) {
    this.length = length;
    this.width = width;
  }
  getArea() {
    return this.length * this.width;
  }
}
class Square extends Rectangle {
  constructor(length) {
    // 调用父类构造函数;子类有 constructor 时必须调用;没有 constructor 则自动调用 super() 并传入所有参数
    super(length, length); // super后再访问this,super可初始化this
  }
  getArea() {
    // 覆盖同名方法
    return 'zilei';
    // return super.getArea(); // 调用父类方法
  }
}
let square = new Square(10);
console.log(square.getArea(), square.length, square.width);

class TestPrototype {
  constructor(){ // function TestPrototype(){}
  this.name = 'xq';
  this.sayInstance = function (){
    console.log('sayInstance');
  };
}
nameProto = 'xqProto';
  sayPrototype(){
    console.log('sayPrototype');
  }
}
const a = new TestPrototype();
const b = new TestPrototype()
a.sayInstance();
a.sayPrototype();
b.sayInstance();
b.sayPrototype();
console.log(a.sayInstance===b.sayInstance, a.sayPrototype===b.sayPrototype);// false true
console.log(a.name===b.name, a.nameProto===b.nameProto);