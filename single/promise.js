/**
 * Promise:Promise是一个对象,里面保存着某个未来才会结束的事件的结果.Promise将异步操作以同步操作的流程表达出来,避免层层嵌套的回调函数.
 * 1.传统异步解决方案:回调函数和事件.
 * 2.Promise对象两个特点:
 *   (1)对象状态不受外界影响,共3种状态:'pending','fulfilled','rejected'.
 *   (2)一旦状态改变,便不会再变.
 * 3.Promise缺点:
 *   (1)无法取消,一旦新建便会执行.
 *   (2)若不设置回调函数,内部抛出的错误,不会反应到外部.
 *   (3)处于'pending'时,无法得知进展到哪一阶段.
 * 4.then方法在当前脚本所有同步任务执行完后才会执行
 * 5.resolve(),rejected()在函数内的末尾执行,与其位置无关,所以其后的代码语句仍会执行,并可能影响其参数(比如引用类型参数).想结束执行可在其前加上'return'.
 * 6.Promise.prototype.catch方法与Promise.prototype.then(null, rejection)相同.建议使用.catch
 */

/*  Promise.prototype.then() 
  该方法返回一个Promise，而它的行为与then中的回调函数的返回值有关：
  如果then中的回调函数返回一个值，那么then返回的Promise将会成为接受状态，并且将返回的值作为接受状态的回调函数的参数值。
  如果then中的回调函数抛出一个错误，那么then返回的Promise将会成为拒绝状态，并且将抛出的错误作为拒绝状态的回调函数的参数值。
  如果then中的回调函数返回一个已经是接受状态的Promise，那么then返回的Promise也会成为接受状态，并且将那个Promise的接受状态的回调函数的参数值作为该被返回的Promise的接受状态回调函数的参数值。
  如果then中的回调函数返回一个已经是拒绝状态的Promise，那么then返回的Promise也会成为拒绝状态，并且将那个Promise的拒绝状态的回调函数的参数值作为该被返回的Promise的拒绝状态回调函数的参数值。
  如果then中的回调函数返回一个未定状态（pending）的Promise，那么then返回Promise的状态也是未定的，并且它的终态与那个Promise的终态相同；同时，它变为终态时调用的回调函数参数与那个Promise变为终态时的回调函数的参数是相同的。
*/
let promise = new Promise((resolve, reject) => {
  console.log('promise');
  let a = [1];
  // throw new Error('!!!');
  // resolve(new Error('????')); // 显式调用resolve()将promise状态从'pending'改为'resolved'
  reject(new Error('????'));
  a[1] = [2]; // resolve(),rejected()在函数内的末尾执行,其后的代码语句仍会执行,并可能影响其参数(比如引用类型参数)
})
  .then(
    resolved => {
      console.log('resolved', resolved);
    },
    rejected => {
      console.log('rejected', rejected);
      // throw new Error('aaa');
      // return undefined;
    }
  )
  .catch(err => {
    // 同Promise.prototype.then(null, rejection)
    console.log('err', err);
  });
console.log(typeof promise);
setTimeout(() => {
  console.log(promise, promise.constructor /* function Promise(){} */);
  promise.then(
    () => {
      console.log(1);
    },
    () => {
      console.log(2);
    }
  );
}, 2000);
