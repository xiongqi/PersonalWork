//默认参数值,传入undefined或者不传参时使用 
function defaultArgs(url, timeout = 2000, callback = function() {}) {
  console.log(arguments.length, arguments);
  console.log(timeout, callback);
}
defaultArgs("url", undefined, ()=>{});

function defaultArgs2(first,second=getValue()){
  console.log(arguments,first+second);
  return first+second;
} 
function getValue(...re){
  return 5;
}
defaultArgs2(1);
defaultArgs2(1,1);

console.log(defaultArgs.length,defaultArgs2.length);
console.log(getValue.length);

function Person(name){
  if(new.target===Person)
    console.log(new.target,typeof new.target);    
  else
    console.log(new.target);
}
function Anotherp(name){
  console.log(new.target);
  Person.call(this,name);
}
var person=new Person("xq");
var anotherp=new Anotherp("another xq");

if(true){
  function test(){
    return 0;
  }
}
console.log(test()); 