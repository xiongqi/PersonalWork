import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { Button } from 'antd';
import AddTodo from '../components/AddTodo';
// import TodoList from '../components/TodoList';
import Filter from '../components/Filter';
import { addTodo, setFilter, setToggle, initial } from '../action/action';
// import store from '../store/store';
import User from '../components/User';
import { fetchJSON, axiosPatchJSON } from '../apis/jserver';
// import Editor from '../components/Editor';
// import Editor2 from '../components/Editor2';

class TodoApp extends React.Component {
  componentWillMount() {
    const fetchPromise = fetchJSON('/apis/data');
    fetchPromise.then(res => res.json()).then(data => {
      this.props.initialStoreData(data.todoList);
    });
  }
  onAddClickAJAX = text => {
    const axiosPromise = axiosPatchJSON('/apis/data', [...this.props.todos, { completed: false, text, key: +new Date() }]);
    axiosPromise.then(res => {
      this.props.onAddClickProp(res.data.todoList);
    });
  };
  render() {
    const { visibleTodoFilter, filterFuncProp, toggle, toggleProp } = this.props;
    const ButtonConf = {
      shape: 'circle',
      htmlType: 'button',
      type: 'primary',
      icon: 'retweet',
      onClick: () => toggleProp(toggle),
    };
    return (
      <div>
        {/* <Editor2 /> */}
        <User />
        <AddTodo onAddClick={this.onAddClickAJAX} />
        <Filter filter={visibleTodoFilter} toggle={toggle} filterFunc={filterFuncProp} />
        {/* <TodoList todos={todos} handleClickList={handleClickListProp} filter={visibleTodoFilter} /> */}
        <div style={{ textAlign: 'right', marginRight: '100px' }}>
          <Link to={toggle ? '/' : '/visual'} activeStyle={{ color: 'red' }}>
            <Button {...ButtonConf} />
          </Link>
        </div>
        {/* <IndexLink to="/" activeStyle={{color:'red'}}>//////</IndexLink> */}
        {this.props.children}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    visibleTodoFilter: state.visibleTodoFilter,
    toggle: state.toggle,
    todos: state.todos,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    initialStoreData: data => dispatch(initial(data)),
    onAddClickProp: text => dispatch(addTodo(text)),
    filterFuncProp: filter => dispatch(setFilter(filter)),
    toggleProp: toggle => dispatch(setToggle(toggle)),
  };
}
const TodoAppContainer = connect(mapStateToProps, mapDispatchToProps)(TodoApp);
export default TodoAppContainer;
