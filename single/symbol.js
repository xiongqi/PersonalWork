let s1 = Symbol();
let s1_description = Symbol('s1_d'); // 参数用来描述此Symbol,存储在内部的[[description]]中,执行toString()方法才可读取.
let s1_2 = Symbol('s1_d');
console.log(s1, s1_description, typeof s1, s1_description===s1_2); // Symbol() Symbol(s1_d) symbol false// 隐式调用 toString()

// Symbol全局注册表: Symbol.for();Symbol.keyFor()
let o1 = Symbol.for('o1');
let o1_2 = Symbol.for('o1');
console.log(o1===o1_2); // true
console.log(Symbol.keyFor(o1), Symbol.keyFor(s1_description)); // o1 undefined

// 字符串属性和Symbol可计算属性
let obj = {};
let ft = Symbol('symbol ft');
obj.ft='first_name'; // . 为字符串属性
obj[ft] = 'symbol_ft';
console.log(obj.ft, obj[ft], obj.ft===obj['ft']); // first_name symbol_ft true

// Object.getOwnPropertySymbols()
let symbolArr = Object.getOwnPropertySymbols(obj);
console.log(symbolArr); // [ Symbol(symbol ft) ]

/**
 * well-known Symbol
 */
// Symbol.hasInstance 定义于 Function.prototype , 用于确定对象是否为函数实例
let re1 = obj instanceof Array; // 等价于 ...
let re2 = Array[Symbol.hasInstance](obj);
console.log(re1, re2); // false false

// Symbol.isConcatSpreadable: 若为true,表示对象有length属性和数字键,它的数值型属性应被独立添加到concat()调用结果中.
let collection = {
  0: 'b',
  1: 'c',
  // length: 2,
  [Symbol.isConcatSpreadable]: true
};
// 1. 无 length 
console.log(['a'].concat(collection)); // Array(1) ['a'] 
// 2. length 偏小
collection.length = 1;
console.log(['a'].concat(collection)); // Array(2) ['a','b'] 
// 3.length 偏大
collection.length = 3;
console.log(['a'].concat(collection)); // Array(4) ['a','b','c'] 

// Symbol.match, Symbol.replace, Symbol.search, Symbol.split, 定义于RegExp.prototype
// 表示相应字符串方法的第一个参数应调用的正则表达式方法

/**
 * Symbol.toPrimitive
 * 1.定义于每一个标准类型的原型上.规定了对象被转换为原始值时应执行的操作.
 * 2.执行原始值转换时,调用Symbol.toPrimitive并传入类型提示参数:'number','string','default'.
 * 3.==,+,Date构造函数传参时,触发默认模式.
 */

 // Sumbol.toStringTag: 定义于Object.prototype,定义了调用对象的 Object.prototype.toString.call()方法时返回的值.
function Person(){}
let me = new Person();
console.log(Object.prototype.toString.call(me), me.toString());
Person.prototype[Symbol.toStringTag] = 'Person';
console.log(Object.prototype.toString.call(me), me.toString());
console.log(Object.prototype.toString.call([]));

// Symbol.unscopables: 对象形式,键为with语句中要忽略的标识符,对应值必须为true.