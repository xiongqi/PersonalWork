// components/test.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    onTap: function () {
      var query = wx.createSelectorQuery().in(this)
      console.log(query)
      console.log(query.select('#tt'))
      console.log(query.selectAll('.tt'))
      console.log(query.selectViewport().boundingClientRect())
    }
  }
})
