import React from 'react';
import PropTypes from 'prop-types';
import Todo from './Todo';
import { Row, Col, Table } from 'antd';
import { completeChange } from '../action/action';
import { connect } from 'react-redux';
import { axiosPatchJSON } from '../apis/jserver';

@connect(mapStateToProps, mapDispatchToProps)
export default class TodoList extends React.Component {
  handleChange = (pagination, filters, sorter) => {
    console.log('\nprops:', this.props, '\npagination:', pagination, '\nfilters:', filters, '\nsorter:', sorter);
  };
  componentDidMount(){
    // this.props.router.setRouteLeaveHook(this.props.route, ()=>'sure to leave?');
  }
  handleClickListAJAX = key => {
    const todos = this.props.todos.map(todo => {
      if (todo.key == key) return Object.assign({}, todo, { completed: !todo.completed });
      return todo;
    });
    const axiosPromise = axiosPatchJSON('/apis/data', todos);
    axiosPromise.then(res=>{
      this.props.handleClickList(res.data.todoList);
    });
  }
  render() {
    // console.log(this.props);
    // let { todos, filter } = this.props;
    let { todos, visibleTodoFilter } = this.props;
    switch (visibleTodoFilter) {
      case 'SHOW_COMPLETED':
        todos = todos.filter(todo => todo.completed);
        break;
      case 'SHOW_ACTIVE':
        todos = todos.filter(todo => !todo.completed);
        break;
      case 'SHOW_ALL':
      default:
        break;
    }
    const columnsConf = [
      { title: 'Thing', dataIndex: 'text', render: (text, record) => <Todo todo={record} onClick={() => this.handleClickListAJAX(record.key)} /> },
      {
        title: 'Date',
        dataIndex: 'key' /* 'date' */,
        width: 200,
        sorter: (a, b) => a.key - b.key,
        /* (old,new)*/ render: key => new Date(key).toLocaleString()
      }
      // sortOrder: this.state.sort?'ascend':'descend', // 默认按 sorter 后降序排列,点击切换亮,并触发onChange //备注,不如不写,自动处理排序
    ];
    const TableConf = {
      dataSource: todos,
      bordered: true,
      columns: columnsConf,
      onChange: this.handleChange
    };
    return (
      <Row>
        <Col span={16} offset={4}>
          <Table {...TableConf} />
        </Col>
      </Row>
    );
  }
}
TodoList.PropTypes = {
  todos: PropTypes.array
};

function mapStateToProps(state) {
  return {
    visibleTodoFilter: state.visibleTodoFilter,
    todos: state.todos
  };
}
function mapDispatchToProps(dispatch) {
  return {
    // onAddClickProp: text => dispatch(addTodo(text)),
    handleClickList: key => dispatch(completeChange(key))
    // filterFuncProp: filter => dispatch(setFilter(filter))
  };
}
// const ToDoListContainer = connect(mapStateToProps, mapDispatchToProps)(TodoList);
// export { ToDoListContainer };
