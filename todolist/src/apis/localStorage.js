const fs = require('fs');

fs.readFile('e:/study/todolist/src/data/TodoList.txt', 'utf-8', (err, data) => {
  if (err) return console.error(err);
  if (!localStorage.length) {
    const str = `{"todos":[${data.toString()}]}`;
    localStorage.todos = JSON.parse(str);
  }
});