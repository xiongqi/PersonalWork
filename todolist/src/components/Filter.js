import React from 'react';
import { Row, Col, Button } from 'antd';

const RowConf = {
  type: 'flex',
  justify: 'center',
  style: { margin: '10px 0', textAlign: 'center' },
};
const ColConf = {
  span: 24,
};
export default class Filter extends React.Component {
  renderFilter = filter => (
    <Button
      style={{ margin: '0 5px 0 0' }}
      disabled={!!this.props.toggle}
      type={filter === this.props.filter ? 'danger' : ''}
      onClick={() => this.props.filterFunc(filter)}
    >
      {filter}
    </Button>
  );

  render() {
    return (
      <Row {...RowConf}>
        <Col {...ColConf}>
          {this.renderFilter('SHOW_ALL')}
          {this.renderFilter('SHOW_COMPLETED')}
          {this.renderFilter('SHOW_ACTIVE')}
        </Col>
      </Row>
    );
  }
}
