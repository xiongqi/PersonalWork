```js
Unicode支持  
1.unicode:  
  (1)'U+'开头表示unicode码,后跟码点.  
  (2)从0开始编号:U+0000 = null.  
  (3)分平面定义,一个平面65536个(2^16);最前面的65536个字符称为基本平面(BMP, Basic Multilingual Plane, U+0000~U+FFFF),放置最常见字符;其余的为辅助平面(SMP, Supplementary Multilingual Plane, U+10000~U+10FFFF).  
  (4)基本平面内,U+D800~U+DFFF是空段.  
2.utf-32:4字节表示,和unicoded完全对应.  
3.utf-16:  
  (1)基本平面码点一一对应,2字节.  
  (2)辅助平面码点,使用4字节,前10位映射到`U+D800~U+DBFF`(2^10),为高位;后10位映射到`U+DC00~U+DFFF`(2^10),为低位.  
  (3)转码公式:  
    H = Math.floor((c-0x10000) / 0x400)+0xD800 (减去0x10000,再除以2^10获得高位,floor向下取整,加上0xD800得到转换值)  
    L = (c - 0x10000) % 0x400 + 0xDC00 (同理)  
4.javascript:只能处理UCS-2编码.  
  (1)UCS-2:2字节编码.后发布的utf-16是ucs-2的超集.  
  (2)采用unicode字符集,但只支持16位的UTF-16编码,不支持32位.  
  (3)ES6:  
    (1)for-of正确判断4字节码点;length为保持兼容,还是原来的错误方式(Array.from(string).length 正确).  
    (2)js允许直接使用码点表示Unicode字符:'\u'+'码点'.4字节码点必须加'{}',2字节可加可不加.  
      (2字节码点: '好'==='\u597D')  
      (4字节码点: '😀'==='\u{1f600}')  
    (3).codePointAt():返回编码单元对应位置的码点.  
       String.fromCodePoint():从Unicode码点返回对应字符.  
       .normalize():Unicode正规化.('\u01D1'.normalize() === '\u004F\u030C'.normalize())(Ǒ ===O(U+004F) + ˇ(U+030C))  
    (4)正则表达式 'u' 修饰符(for Unicode).
```